<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Fatura', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->unsignedBigInteger('clienteFatura');
            $table->foreign('clienteFatura')->references('idPessoa')->on('Pessoa');
            $table->string('ipClienteFatura');
            $table->float('valorFatura');
            $table->text('obsFatura');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Fatura');
    }
}
