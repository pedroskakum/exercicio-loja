<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePessoaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pessoa', function(Blueprint $table){
            $table->bigIncrements('idPessoa');
            $table->string('nomePessoa');
            $table->string('emailPessoa');
            $table->string('senhaPessoa');
            $table->boolean('statusPessoa');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Pessoa');
    }
}
