<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Categoria;
use App\Fatura;
use App\Pessoa;
use App\Produto;

$factory->define(Pessoa::class, function (Faker\Generator $faker) {
    return [
        'nomePessoa' => $faker->name,
        'emailPessoa' => $faker->safeEmail,
        'senhaPessoa' => bcrypt(str_random(10)),
        'bool' => $faker->boolean(80),
    ];
});

$factory->define(Produto::class, function (Faker\Generator $faker) {

    $categoria = factory(App\Categoria::class)->create();

    return [
        'idCategoria' => $categoria->id,
        'fotoProduto' => $faker->imageUrl(),
        'nomeProduto' => $faker->words(2),
        'precoProduto' => $faker->randomFloat(2, 10, 100),
        'estoqueProduto' => $faker->randomDigit,
        'destaqueProduto' => $faker->boolean(20),
    ];
});

$factory->define(Categoria::class, function (Faker\Generator $faker) {

    $categoria = factory(App\Categoria::class)->create();

    return [
        'idCategoriaPai' => $categoria->id,
        'nomeCategoria' => $faker->word,
    ];
});

$factory->define(Fatura::class, function (Faker\Generator $faker) {
    return [
        'clienteFatura' => $faker->words(5),
        'ipClienteFatura' => $faker->ipv4,
        'float' => $faker->randomFloat(2, 50, 150),
        'obsFatura' => $faker->paragraphs
    ];
});