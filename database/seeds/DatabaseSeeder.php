<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Categoria::class)->create();
        /*factory(App\Pessoa::class, 10)->create()->each(function($Pessoa){
            factory(App\Fatura::class, 10)->create(['clienteFatura' => $Pessoa->id]);
        });

        factory(App\Categoria::class, 10)->create()->each(function($CategoriaPai)
        {
            factory(App\Categoria::class, 5)->create(['idCategoriaPai' => $CategoriaPai->id])->each(function($Categoria)
            {
                factory(App\Produto::class, 10)->create(['idCategoria' => $Categoria->id]);
            });
        });*/
    }
}
