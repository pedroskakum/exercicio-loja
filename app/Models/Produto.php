<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    public $timestamps = false;
    protected $table = 'Produto';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'idCategoria', 'fotoProduto', 'nomeProduto', 'precoProduto', 'estoqueProduto', 'detaqueProduto'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Categoria() {
        return $this->belongsTo(Categoria::class, 'idCategoria');
    }
}
