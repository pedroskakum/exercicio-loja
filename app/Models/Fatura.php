<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fatura extends Model
{
    public $timestamps = false;
    protected $table = 'Fatura';
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'clienteFatura', 'valorFatura', 'obsFatura'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function Pessoa() {
        return $this->belongsTo(Pessoa::class, 'clienteFatura');
    }
}
