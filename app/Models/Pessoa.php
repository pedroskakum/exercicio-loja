<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class Pessoa extends Authenticatable
{
    public $timestamps = false;
    protected $table = 'Pessoa';
    protected $primaryKey = 'idPessoa';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idPessoa', 'nomePessoa', 'emailPessoa', 'statusPessoa'
    ];

    /**\
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'senhaPessoa',
    ];
}
