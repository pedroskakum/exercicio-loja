<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('', 'LandingController@index');

Route::resource('categoria', 'CategoriaController');
Route::resource('produto', 'ProdutoController');
Route::resource('carrinho', 'CarrinhoController');

Route::group(['middleware' => 'auth'], function () {

    Route::resource('dashboard', 'DashboardController', ['only' => 'index']);
    Route::group(['prefix' => 'dashboard'], function () {

        Route::resource('produtos', 'Dashboard/ProdutoController');
        Route::resource('categorias', 'Dashboard/CategoriaController');
        Route::resource('logout', 'Dashboard/LogoutController', ['only' => ['index']]);

    });
});